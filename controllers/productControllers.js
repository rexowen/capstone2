const Product = require('../models/Product');

//Create Product-
module.exports.addProduct = (data) => {
	if(data.isAdmin){
		let newProduct = new Product({
			name: data.product.name,
			description: data.product.description,
			price: data.product.price
		})
		console.log(data);
		return newProduct.save().then((product, error) => {
			if(error){
				return false;
			}else{
				return true;
			}
		})
	}else{
		return false;
	}
}

//Update A Product-
module.exports.updateProduct = (reqParams, reqBody) => {
		let updatedProduct = {
			name: reqBody.name,
			description: reqBody.description,
			price: reqBody.price
		};
		return Product.findByIdAndUpdate(reqParams.productId, updatedProduct).then((product, error) => {
		if(error){
			return false;
		}else{
			return product;
		}
	})
}

//Archive a Product
module.exports.archiveProduct = (reqParams) =>  {

		let updatedProduct = {
			"isActive": false
		};
		return Product.findByIdAndUpdate(reqParams.productId, updatedProduct).then((product, error) => {
		if(error){
			return false;
		}else{
			return true;
		}
	})
}


//Retrieve Single Product
module.exports.specificProduct = (productId) =>{
	return Product.findById(productId)
}


//Retrieve all ACTIVE Products
module.exports.getAllActive = () => {
	return Product.find({isActive: true}).then(result => {
		return result;
	})
}

//Delete Product
module.exports.deleteProduct = (productId) => {
	return Product.findByIdAndRemove(productId).then((removedProduct, err) => {
		if(err){
			console.log(err)
			return false;
		}else{
			return removedProduct;
		}
	})
}