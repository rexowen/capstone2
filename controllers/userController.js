const User = require('../models/User');
const Product = require('../models/Product');
const bcrypt = require('bcrypt');
const auth = require('../auth');


//User Registration-
module.exports.registerUser = (reqBody) => {
	let newUser = new User( {
		firstName: reqBody.firstName,
		lastName: reqBody.lastName,
		email: reqBody.email,
		password: bcrypt.hashSync(reqBody.password, 10)
	})
	return newUser.save().then((user, error) => {
		if(error){
			return false;
		}else{
			return true;
		}
	})
}


//User Authentication/LogIn-
module.exports.loginUser = (reqBody) => {
	return User.findOne( {email: reqBody.email} ).then(result => {
		if(result == null){
			return false;
		}else{
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password)
			if(isPasswordCorrect){
				return { accessToken: auth.createAccessToken(result.toObject()) }
			}else{
				return false;
			}

		}
	})
}


//Set as Admin
module.exports.setAsAdmin = (reqParams) => 
	{
		let updatedUser = {
			"isAdmin": true
		};
		return User.findByIdAndUpdate(reqParams.userId, updatedUser).then((user, error) => {
		if(error){
			return false;
		}else{
			return user;
		}
	})
}


//Non Admin Purchase
module.exports.checkout = async (data) => {
	let isUserUpdated = await User.findById(data.userId).then(user => {
		user.orders.push({ productId: data.productId, TotalAmount: data.productPrice});
	return user.save().then((user, error) => {
		if(error){
			return false;
		}else{
			return true;
		}
	})
	})
	let isProductUpdated = await Product.findById(data.productId).then(product => {
		product.userPurchased.push({ userId: data.userId });
		return product.save().then((product, error) => {
			if(error){
				return false;
			}else{
				return true;
			}
		})
	})
	if(isUserUpdated && isProductUpdated){
		return true;
	}else{
		return false;
	}
}


//Non Admin Orders
module.exports.getMyOrders = (data) => {
	return User.findById(data.userId).then(result => {
		return result.orders;
	})
}


// Admin Orders
module.exports.getAllOrders = (data) => {	
			return User.find(data.user).then(result => {
		return result;
	})
}

//Delete User
module.exports.deleteUser = (userId) => {
	return User.findByIdAndRemove(userId).then((removedUser, err) => {
		if(err){
			console.log(err)
			return false;
		}else{
			return removedUser;
		}
	})
}