const express = require('express');
const router = express.Router();
const auth = require('../auth');
const productController = require('../controllers/productControllers');


//Create Product-
router.post('/', auth.verify, (req, res) => {
	const data = {
		product: req.body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}
	productController.addProduct(data).then(result => res.send(result));
})

//Update Product
router.put('/:productId/update', auth.verify, (req, res) => {

	const data = {
		product: req.body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}
	if(data.isAdmin){
	productController.updateProduct(req.params, req.body).then(result => res.send(result));
	}else{
		res.send(false);
	}
})

//Archive Product
router.put('/:productId/archive', auth.verify, (req, res) => {
	const data = {
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}
	if(data.isAdmin){
	productController.archiveProduct(req.params).then(result => res.send(result));
	}else{
		res.send(false)
	}
})

//Retrieve Single Product-
router.get("/:id", auth.verify, (req,res)=>{


	const userData = auth.decode(req.headers.authorization);
	productController.specificProduct(req.params.id)
	.then(result =>{
		res.send(result)
	})
})

//Retrieve all Active Products-
router.get('/', auth.verify, (req, res) => {

	const userData = auth.decode(req.headers.authorization);
	productController.getAllActive().then(result => res.send(result));
})

//Delete Product
router.delete('/:id', (req, res) => {
	const data = {
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}
	if(data.isAdmin){
		productController.deleteProduct(req.params.id).then(result => res.send(result));
		}else{
			res.send(false)
		}
})


module.exports = router;