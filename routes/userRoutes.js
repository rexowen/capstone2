const express = require('express');
const router = express.Router();
const userController = require('../controllers/userController');
const auth = require('../auth');

//User Registration-
router.post('/register', (req, res) => {
	userController.registerUser(req.body).then(result => res.send(result));
})

//User LogIn-
router.post('/login', (req, res) => {
	userController.loginUser(req.body).then(result => res.send(result));
})

//User Admin-
router.put('/:userId/setAsAdmin', auth.verify, (req, res) => {

	const userData = auth.decode(req.headers.authorization);
	userController.setAsAdmin(req.params).then(result => res.send(result));
})

//Non Admin Purchase-
router.post('/checkout', auth.verify, (req, res) => {
	let data = {
		userId: auth.decode(req.headers.authorization).id,
		productId: req.body.productId,
		productPrice: req.body.price
	}
	userController.checkout(data).then(result => res.send(result))
})

//Non Admin Get All Orders
router.get('/myOrders', auth.verify, (req, res) => {
	const data = {
		isAdmin: auth.decode(req.headers.authorization).isAdmin,
		userId: auth.decode(req.headers.authorization).id
	}
	if(data.isAdmin){		
	res.send(false);
	}else{
	userController.getMyOrders(data).then(result => res.send(result));
}
})

//Admin Orders
router.get('/orders', auth.verify, (req, res) => {
	const data = {
		isAdmin: auth.decode(req.headers.authorization).isAdmin,
		user: auth.decode(req.headers.authorization).user,
	}
	if(data.isAdmin){
	userController.getAllOrders(data).then(result => res.send(result));
	}else{
		res.send(false)
	}
})

router.delete('/:id', (req, res) => {
	const data = {
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}
	if(data.isAdmin){
		userController.deleteUser(req.params.id).then(result => res.send(result));
		}else{
			res.send(false)
		}
})




module.exports = router;